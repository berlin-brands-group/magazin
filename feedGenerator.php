<?php

require_once 'vendor/autoload.php';

use Sunra\PhpSimple\HtmlDomParser;

$start_time = microtime(true);

$source = str_replace(["\n"], [''], file_get_contents('https://magazin.klarstein.com/sk/feed/'));

$xml = simplexml_load_string($source, 'SimpleXMLElement', LIBXML_NOCDATA);

$output = [];

foreach($xml->channel->item as $key => $value) {
	$artTitle = (string)$value->title;
	$artTimestamp = strtotime(str_replace([", ", " +0000"], "", strstr((string)$value->pubDate, ", ", false)));
	$artLink = (string)$value->link;
	$artContent = (string)$value->children('content', true)->encoded;

	$parser = HtmlDomParser::str_get_html($artContent);
	$artImage = $parser->find('img', 0)->src;
	$artPerex = $parser->find('p', 0)->plaintext;

	$output[$artTimestamp] = ["title" => $artTitle, "link" => $artLink, "image" => $artImage, "perex" => $artPerex];
}

$count = 0;

$dom = new \DOMDocument('1.0', 'utf-8');
$articles = $dom->createElement('articles');

foreach($output as $artKey => $artValue) {

	$article = $dom->createElement('article');

	$isNewest = 0;
	if($count === 0)
		$isNewest = 1;

	$articleId = $dom->createElement('id', $artKey);
	$article->appendChild($articleId);

	$articleTitle = $dom->createElement('title', $artValue['title']);
	$article->appendChild($articleTitle);

	$articleLink = $dom->createElement('link', $artValue['link']);
	$article->appendChild($articleLink);

	$articleImage = $dom->createElement('image', $artValue['image']);
	$article->appendChild($articleImage);

	$articlePerex = $dom->createElement('perex', $artValue['perex']);
	$article->appendChild($articlePerex);

	$articleCreated = $dom->createElement('created', date('Y-m-d H:i:s', $artKey));
	$article->appendChild($articleCreated);

	$articleInNewest = $dom->createElement('is_newest', $isNewest);
	$article->appendChild($articleInNewest);

	$articles->appendChild($article);
	$count += 1;

}
$dom->appendChild($articles);
$save = file_put_contents('magazin_article_feed.xml', $dom->saveXML());

if($save === FALSE) {
	print "Magazin article feed error!\n";
}
else {
	print "Overall article: " . $count ."\n";
}

$end_time = microtime(true); 
$execution_time = $end_time - $start_time; 
echo "Execution time: " . round($execution_time, 1) . " sec\n";